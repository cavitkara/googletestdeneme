
cmake_minimum_required(VERSION 3.1...3.22)

project(
  ModernCMakeExampleTest
  VERSION 1.0
  LANGUAGES CXX)



add_executable(SimpleTest test.cpp)


 target_link_libraries(SimpleTest gtest gmock gtest_main MatrixMultiplication BinomLib SpreadSheetLib)
   
 gtest_discover_tests(SimpleTest
        # set a working directory so your project root so that you can find test data via paths relative to the project root
        WORKING_DIRECTORY ${PROJECT_DIR}
        PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${PROJECT_DIR}")
 
 set_target_properties(${TESTNAME} PROPERTIES FOLDER tests)
#include "Matrix.h"
#include "Strassen.h"
#include "gtest/gtest.h"
#include "BinomialCoefficient.h"
#include "Table.h"
#include "Util.h"


TEST(MultiplicationTest, OrdinaryValues) {

	bool randomGenerate = true;
	int n = 4096;

	Matrix m1(n, 0, randomGenerate);
	Matrix m2(n, 0, randomGenerate);
	

	auto c1 = m1 * m2;
	auto c2 = optimizedMultiply(m1, m2, n);

  EXPECT_TRUE( c1 == c2 );
}

TEST(BinomialTest, OrdinaryValues) {

	 int a1 = binomialCoeff(3, 1);
	 int a2 = calcBinomCoeffs<3, 1>();

	EXPECT_EQ(a1 , a2);

	a1 = binomialCoeff(5, 0);
	a2 = calcBinomCoeffs<5, 0>();

	EXPECT_EQ(a1, a2);
}


TEST(SpreadSheetTest, Example1)
{
	Table table;
	table.readInput("InputTestFile1.txt");
	table.run();
	table.printTableOutput2File("OutputTestFile1.txt");
	bool res = compareFiles("ExpectedOutputTestFile1.txt" , "OutputTestFile1.txt");
	EXPECT_EQ(0, 0);
}
#pragma once


class Matrix;

Matrix optimizedMultiply(  const Matrix& M1, const Matrix& M2, int k);

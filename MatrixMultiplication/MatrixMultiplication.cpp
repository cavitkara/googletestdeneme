// MatrixMultiplication.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Matrix.h"
#include "Strassen.h"

using namespace std;

int main()
{
	Matrix m1(2);
	m1.at(0, 0) = 1;
	m1.at(0, 1) = 2;
	m1.at(1, 0) = 3;
	m1.at(1, 1) = 4;

	Matrix m2(2, 2);
	m2.at(0, 1) = 0;
	m2.at(1, 0) = 1;

	auto m3 = m1 * m2;
	cout <<"m3:\n" <<m3;

	auto m4 = optimizedMultiply(m1 ,m2, 2);
	cout << "m4:\n" << m4;
}



#pragma once
#include <vector>

class Matrix
{

	friend std::ostream& operator<<(std::ostream& os, const Matrix& obj);
public:

	//Matrix(int n, double val = 0.0);
	Matrix(int n, double val = 0.0, bool rand = false);
	~Matrix();

	Matrix operator+(const Matrix& m1) const;
	Matrix operator-(const Matrix& m1) const;
	Matrix operator*(const Matrix& m1) const;
	bool operator==(const Matrix& m1) const;

	int Size() const ;
	double& at(int x, int y) const;

	private:
		int size = 1;
		double *array;

		size_t index(int x, int y) const;

};


#include "Matrix.h"
#include <iostream>
#include <random>

//Matrix::Matrix(int n, double value): size{n}
//{
//	
//}

Matrix::Matrix(int n, double value, bool _rand):size { n }
{

	array = new double[n*n];

	if(!_rand)
	{
		
		std::fill(array, array + n * n, value);
	}
	else
	{
		std::random_device dev;
		std::mt19937 rng(dev());
		std::uniform_int_distribution<std::mt19937::result_type> dist10(1, 10); // distribution in range [1, 10]

		for(int i=0; i< n; ++i)
		{
			for(int j=0; j<n; ++j)
			{
				array[index(i,j)] = dist10(rng);
			}
		}
	}
	

	
}

Matrix::~Matrix()
{
	//delete[] array;
}



Matrix Matrix::operator+(const Matrix & m1) const
{
	if(size != m1.Size())
	{
		std::cout << "Matrix sizes are not equal for sum operation!!\n";
		return *this;
	}
	Matrix c(size);

	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size; j++)
		{
			c.array[c.index(i, j)] = this->at(i, j) + m1.at(i, j);
		}
	}

	return c;
}

Matrix Matrix::operator-(const Matrix & m1) const
{
	if (size != m1.Size())
	{
		std::cout << "Matrix sizes are not equal for sum operation!!\n";
		return *this;
	}
	Matrix c(size);

	for (int i = 0; i < size; i++)
	{
		for (int j = 0; j < size; j++)
		{
			c.array[c.index(i, j)] = this->at(i, j) - m1.at(i, j);
		}
	}

	return c;
}

Matrix Matrix::operator*(const Matrix & m1) const
{
	Matrix c(size);

	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size; j++)
		{
			for(int k=0; k<size; k++)
			{
				c.at(i, j) += at(i, k)* m1.at(k, j);
			}
		}
	}
	return c;
}

bool Matrix::operator==(const Matrix & m1) const
{
	if(size != m1.size)
	{
		return false;
	}

	for(int i=0; i<size; i++)
	{
		for(int j=0; j<size;++j)
		{
			if( array[index(i, j)] != m1.array[index(i, j)] )
			{
				return false;
			}
		}
	}
	return true;
}

int Matrix::Size() const
{
	return size;
}

double& Matrix::at(int x, int y) const
{
	return array[index(x,y)];
}

size_t Matrix::index(int x, int y) const
{
	return(x + size * y);
}

std::ostream & operator<<(std::ostream& os, const Matrix & obj)
{
	for(int i=0; i< obj.size; i++)
	{
		for(int j=0; j<obj.size; j++)
		{
			os << obj.at(i, j) << " ";
		}
		os << std::endl;
	}
	os << std::endl;
	return os;
}

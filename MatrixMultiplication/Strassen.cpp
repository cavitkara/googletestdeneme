#include "Strassen.h"
#include  "Matrix.h"

Matrix optimizedMultiply(const Matrix& A, const Matrix& B, int n)
{
	
	if(n <= 2)
	{
		Matrix C(n);
		return C = A * B;
	}

	const auto k = n / 2;
	Matrix A11(k), A22(k), A21(k), A12(k), B11(k), B21(k), B12(k), B22(k);

	for(int i=0; i<k; i++)
		for(int j=0; j<k; j++)
		{
			A11.at(i, j) = A.at(i, j);
			A12.at(i, j) = A.at(i, k+j);
			A21.at(i, j) = A.at(k+i, j);
			A22.at(i, j) = A.at(k+i, k+j);
			B11.at(i, j) = B.at(i, j);
			B12.at(i, j) = B.at(i, k+j);
			B21.at(i, j) = B.at(k+i, j);
			B22.at(i, j) = B.at(k+i, k+j);
		}

	Matrix P1 = optimizedMultiply(A11, (B12 - B22), k);
	Matrix P2 = optimizedMultiply((A11 + A12), B22, k);
	Matrix P3 = optimizedMultiply((A21 + A22), B11, k);
	Matrix P4 = optimizedMultiply(A22, (B21 - B11), k);
	Matrix P5 = optimizedMultiply((A11 + A22), (B11 + B22), k);
	Matrix P6 = optimizedMultiply((A12 - A22), (B21 - B22), k);
	Matrix P7 = optimizedMultiply((A11 - A21 ), (B11 + B12), k);

	Matrix C11 = (((P5 + P4) + P6) - P2);
	Matrix C12 = (P1 + P2);
	Matrix C21 = (P3 + P4);
	Matrix C22 = (((P5 + P1) - P3) - P7);

	Matrix C(k);

	for (int i = 0; i < k; i++)
		for (int j = 0; j < k; j++) 
		{
			C.at(i,j) = C11.at(i,j);
			C.at(i, j + k) = C12.at(i, j);
			C.at(k + i,j) = C21.at(i,j);
			C.at(k + i, k + j) = C22.at(i, j);
		}

	return C;
}

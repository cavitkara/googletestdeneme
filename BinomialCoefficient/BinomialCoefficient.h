#pragma once



/**
 * \brief  Original binomial coeffs calculator function. Used as a ground truth function
 * \param n elements of set
 * \param k number of choice in n elements
 * \return Returns value of Binomial Coefficient C(n, k).
 */
int binomialCoeff(int n, int k);

/**
 * \brief   Optimized binomial coefficient calculator function. Since binomial coeffs are constant and dependent on their previous values, they are calculated at compile time. 
 * \tparam N Number of elements in the set
 * \tparam K Number of the  chosen elements out of N
 * \return 
 */

template<int N, int K>
constexpr int calcBinomCoeffs()
{
	if constexpr (K == 0)
		return 1;

	if constexpr (N >= 0 && N<=1 )
		return 1;

	if constexpr (N == K)
		return 1;
	
	if constexpr (N > 1 && K>0) 
		return calcBinomCoeffs<N - 1, K>() + calcBinomCoeffs<N - 2, K>();

	return 1;
	
}


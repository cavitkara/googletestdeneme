#include <iostream>

using namespace std;



int binomialCoeff(int n, int k)
{
	int res = 1;

	//C(n, k) = C(n, n-k)
	if (k > n - k)
		k = n - k;

	
	// [n * (n-1) ... (n-k+1)] / [k * (k-1) ... 1]
	for (int i = 0; i < k; ++i) {
		res *= (n - i);
		res /= (i + 1);
	}

	return res;
}


//// Driver Code
//int main()
//{
//	constexpr int n = 0, k = 1;
//	cout << "Value of C(" << n << ", "<< k << ") is " << binomialCoeff(n, k) << endl;
//
//	cout << "Value of C(" << n << ", " << k << ") is " << calcBinomCoeffs<3,1>();
//
//	return 0;
//}

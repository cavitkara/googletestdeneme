#pragma once
#include <string>


enum class CellType
{
	Empty,
	Constant,
	Formula,
	Error
};


class Cell
{

public:
	Cell(const std::string& str);
	const std::string& getRawValue() const;
	CellType getType();
	void setResolvedValue(const std::string&);
	std::string getResolvedValue();
	/**
	 * \brief it is resolved when it becomes constant equvalent
	 */
	bool resolved = false;

private:

	/**
	 * \brief
	 * \return Determines the types of the cell when it's constructed or added
	 */
	void determineCellType(const std::string& str) ;

	CellType type = CellType::Empty;
	std::string rawValue;
	std::string resolvedValue;

};


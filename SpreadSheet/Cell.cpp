#include "Cell.h"
#include <locale>
#include <cctype>
#include "Util.h"

Cell::Cell(const std::string& data): rawValue(data)
{
	determineCellType(data);
	if(type == CellType::Constant)
	{
		resolvedValue = data;
		resolved = true;
	}
	else if(type == CellType::Error)
	{
		resolvedValue = "#ERROR";
		
	}
	else if(type == CellType::Empty)
	{
		resolvedValue = "#NAN";
		
	}

}

const std::string & Cell::getRawValue() const
{
	return rawValue;
}

CellType Cell::getType()
{
	return type;
}

void Cell::setResolvedValue(const std::string &str)
{
	resolvedValue = str;
}

std::string Cell::getResolvedValue()
{
	return resolvedValue;
}

void Cell::determineCellType(const std::string & str) 
{
	if (isnumber(str))
	{
		type = CellType::Constant;
	}
	else if (str.length() > 0)
	{
		if (str[0] == '=')
		{
			type = CellType::Formula;
		}
		else if(str == "#NAN")
		{
			type = CellType::Empty;
			resolvedValue = str;
		}

	}
	else
	{
		type = CellType::Empty;
	}
}

#include "Table.h"
#include <iostream>
#include "Cell.h"
#include <memory>
#include <sstream>
#include <vector>
#include <regex>
#include "Util.h"
#include <fstream>

Table::Table()
{
	initTable();
}

void Table::run()
{
	for (int i = 1; i <= Table::max_row; i++)
		for (int j = 'A'; j <= 'Z'; j++)
		{
			auto row_col = std::string(1, j) + std::to_string(i);
			auto cell  = data[row_col];

			if(cell->getType() == CellType::Constant)
			{
				cell->setResolvedValue(cell->getRawValue());
			}
			else if(cell->getType() == CellType::Error)
			{
				cell->setResolvedValue("#ERROR");
			}
			else if (cell->getType() == CellType::Formula)
			{
				std::string cellString = cell->getRawValue();
				cellString = cellString.substr(1, cellString.length() - 1); //Exclude '='
				std::string result;
				std::set<std::string> previousRefs;
				previousRefs.insert(row_col);
				result = resolveReferences(row_col, previousRefs);
				data[row_col]->setResolvedValue(result);
				data[row_col]->resolved = true;
			}
		}
}

void Table::addCell(const std::string& rowData, const std::string & rowCol)
{
	auto cellPtr = std::make_shared<Cell>(rowData);
	data[rowCol] = cellPtr;
}

std::shared_ptr<Cell> Table::getCell(const std::string &str) 
{
	return data[str];
}

void Table::printTableOutput4Debug()
{
	std::cout << '\t'; // for column of row index
	for (int i = 'A'; i <= 'Z'; i++)
	{
		std::cout << static_cast<char>(i) << '\t';
		std::flush(std::cout);
	}

	std::cout << std::endl;

	for (int i = 1; i <= max_row; i++)
	{
		std::cout << std::to_string(i) << "\t";
		std::flush(std::cout);

		for (int j = 'A'; j <= 'Z'; j++)
		{

			auto row_col = std::string(1, j) + std::to_string(i);

			/*if (obj.data.find(row_col) == obj.data.end())
			{
				std::cout << '\t';
				continue;
			}*/
			auto cell = data[row_col];

			if (!cell->resolved)
			{
				std::cout << '\t';
				std::flush(std::cout);
				continue;
			}

			if (cell->getType() == CellType::Empty)

			{
				std::cout << '\t';
				std::flush(std::cout);
				continue;
			}
			else if (cell->getType() == CellType::Error)
			{
				std::cout << "#ERROR\t";
				std::flush(std::cout);
				continue;
			}
			else //if(cell->getType() == CellType::Constant || cell->getType() == CellType::Formula)
			{
				std::cout << data[row_col]->getResolvedValue() << '\t';
				std::flush(std::cout);
			}
		}
		std::cout << std::endl;
	}
}

void Table::printTableOutput()
{


	std::cout << "\n-----Output-----"<<std::endl;
	std::flush(std::cout);


	for (int i = 1; i <= max_row; i++)
	{
		

		for (int j = 'A'; j <= 'Z'; j++)
		{

			auto row_col = std::string(1, j) + std::to_string(i);

			/*if (obj.data.find(row_col) == obj.data.end())
			{
				std::cout << '\t';
				continue;
			}*/
			auto cell = data[row_col];

			if (!cell->resolved)
			{
				std::cout << '\t';
				std::flush(std::cout);
				continue;
			}

			if (cell->getType() == CellType::Empty)

			{
				std::cout << '\t';
				std::flush(std::cout);
				continue;
			}
			else if (cell->getType() == CellType::Error)
			{
				std::cout << "#ERROR\t";
				std::flush(std::cout);
				continue;
			}
			else //if(cell->getType() == CellType::Constant || cell->getType() == CellType::Formula)
			{
				std::cout << data[row_col]->getResolvedValue() << '\t';
				std::flush(std::cout);
			}
		}
		std::cout << std::endl;
	}
}

void Table::printTableOutput2File(const std::string& str) 
{
	std::ofstream file;
	file.open(str, std::ofstream::out);

	if(file.is_open())
	{
		
		for (int i = 1; i <= max_row; i++)
		{


			for (int j = 'A'; j <= 'Z'; j++)
			{

				auto row_col = std::string(1, j) + std::to_string(i);

				/*if (obj.data.find(row_col) == obj.data.end())
				{
					std::cout << '\t';
					continue;
				}*/
				auto cell = data[row_col];

				if (!cell->resolved)
				{
					file << '\t';
					std::flush(file);
					continue;
				}

				if (cell->getType() == CellType::Empty)

				{
					file << '\t';
					std::flush(file);
					continue;
				}
				else if (cell->getType() == CellType::Error)
				{
					file << "#ERROR\t";
					std::flush(file);
					continue;
				}
				else //if(cell->getType() == CellType::Constant || cell->getType() == CellType::Formula)
				{
					file << data[row_col]->getResolvedValue() << '\t';
					std::flush(file);
				}
			}
			file << std::endl;
		}

	}

	file.close();
}

void Table::readInput()
{
	std::string line;

	while(std::cin)
	{
		std::getline(std::cin, line);
		char delimiter = '\t';
		std::string token;
		std::istringstream split(line);

		for (std::string each; std::getline(split, each, delimiter); )
		{
			token = std::move(each);
			auto rowCol = std::string(1, columnCount) + std::to_string(rowCount);
			addCell(token, rowCol);
			++columnCount;

			if(columnCount > max_column)
			{
				break;
			}
		}

		columnCount = 'A';
		++rowCount;

		if(rowCount > 3)
		{
			break;
		}
	}
}

void Table::readInput(const std::string& input)
{
	std::ifstream file(input);
	std::string line;

	if(file.is_open())
	{
		std::cin.rdbuf(file.rdbuf());
		while (std::getline(file, line))
		{
			char delimiter = '\t';
			std::string token;
			std::istringstream split(line);

			for (std::string each; std::getline(split, each, delimiter); )
			{
				token = std::move(each);
				auto rowCol = std::string(1, columnCount) + std::to_string(rowCount);
				addCell(token, rowCol);
				++columnCount;

				if (columnCount > max_column)
				{
					break;
				}
			}

			columnCount = 'A';
			++rowCount;

			if (rowCount > 3)
			{
				break;
			}
		}
	}

}


std::string Table::resolveReferences(const std::string & input, std::set<std::string>& prevs)
{
	auto cell = data[input];

	if(cell->resolved == true)
	{
		return cell->getResolvedValue();
	}

	if (cell->getType() == CellType::Constant) // 5
	{
		cell->setResolvedValue(cell->getRawValue());
		cell->resolved = true;
		return cell->getResolvedValue();
	}
	else if (cell->getType() == CellType::Error) // ?
	{
		cell->setResolvedValue("#ERROR");
		cell->resolved = true;
		return cell->getResolvedValue();

	}
	else if (cell->getType() == CellType::Empty)  // ""
	{
		
		/*cell->resolved = true;
		cell->setResolvedValue("#NAN");*/
		return std::string("#NAN");
	}
	else if (cell->getType() == CellType::Formula) // =B1+2*A2
	{
		std::string cellString = cell->getRawValue();
		cellString = cellString.substr(1, cellString.length() - 1); //Exclude '='

		std::regex re("[+-/*]");
		std::sregex_token_iterator first{ cellString.begin(), cellString.end(), re, -1 }, last; //the '-1' is what makes the regex split (-1 := what was not matched)
		std::vector<std::string> tokens{ first, last };
		std::vector<std::string> resolvedTokens;

		int result = 0;

		for (auto t : tokens) 
		{

			if (isnumber(t)) //Constant
			{
				resolvedTokens.push_back(t);
				
			}
			else //Reference
			{
				if (prevs.find(t) != prevs.end())
				{
					cell->setResolvedValue("#ERROR");
					cell->resolved = true;
					return cell->getResolvedValue();
				}

				prevs.insert(t);
				std::string resultValue = resolveReferences(t, prevs);
				prevs.erase(t);

				if (resultValue == "#NAN")
				{
					//cell->resolved = true;
					return std::string("#NAN") ;
				}

				if(resultValue == "#ERROR"  )
				{
					cell->setResolvedValue(resultValue);
					cell->resolved = true;
					return resultValue;
				}
				resolvedTokens.push_back(resultValue);
			}
		}
		std::string newResult;
		for (int i = 0, j = 0; i < cellString.length(); i++)
		{
			if (cellString[i] == '+' || cellString[i] == '-' || cellString[i] == '*' || cellString[i] == '/')
			{
				newResult.append(resolvedTokens[j]) ;
				newResult += cellString[i];
				++j;
			}
		}
		newResult.append(resolvedTokens.back()); //Last element without math operator

		int cal = eval(newResult);
		cell->setResolvedValue(std::to_string(cal));
		cell->resolved = true;
		return cell->getResolvedValue();
	}

	return std::string("#NAN");
}

void Table::initTable()
{
	for (int i = 1; i <= max_row; i++)
		for (int j = 'A'; j <= 'Z'; j++)
		{
			auto row_col = std::string(1, j) + std::to_string(i);
			auto newCell = std::make_shared<Cell>("");
			data[row_col] = newCell;
		}
}

std::ostream & operator<<(std::ostream & os,  Table & obj)
{
	os << '\t'; // for column of row index
	for (int i = 'A'; i <= 'Z'; i++)
	{
		os << static_cast<char>(i)<< '\t';
		std::flush(os);
	}

	os << std::endl;

	for (int i = 1; i <= obj.max_row; i++)
	{
		os << std::to_string(i) << "\t";
		std::flush(os);
		
		for (int j = 'A'; j <= 'Z'; j++)
		{
			
			auto row_col = std::string(1, j) + std::to_string(i);
			
			/*if (obj.data.find(row_col) == obj.data.end())
			{
				std::cout << '\t';
				continue;
			}*/
			auto cell = obj.data[row_col];

			if(cell->getType() == CellType::Empty)

			{
				os << '\t';
				std::flush(os);
				continue;
			}
			else if(cell->getType() == CellType::Error)
			{
				os <<"#ERROR\t";
				std::flush(os);
				continue;
			}
			else //if(cell->getType() == CellType::Constant || cell->getType() == CellType::Formula)
			{
				os << obj.data[row_col]->getRawValue() << '\t';
				std::flush(os);
			}
		}
		os << std::endl;
	}
	return os;
}

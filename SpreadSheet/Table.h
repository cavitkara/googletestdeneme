#pragma once
#include<memory>
#include <string>
#include <map>
#include <set>

class Cell;
class Matrix;
enum class CellType;

class Table
{

public:

	/**
	 * \brief Default constructor
	 */
	Table();

	void run();

	/**
	 * \brief Prints input values
	 * \param os 
	 * \param obj 
	 * \return 
	 */
	friend std::ostream& operator<<(std::ostream& os,  Table& obj);
	/**
	 * \brief Adds cell into table
	 */
	void addCell(const std::string&, const std::string& rowCol);
	/**
	 * \brief 
	 * \return retreives cell from table
	 */
	std::shared_ptr<Cell> getCell(const std::string&);  
	/**
	 * \brief Prints output with column and row numbers as A1,C3 etc
	 */
	void printTableOutput4Debug();

	/**
	 * \brief Prints output as expected format
	 */
	void printTableOutput();

	/**
	 * \brief Prints output to file for unit tests
	 */
	void printTableOutput2File(const std::string& str) ;

	void readInput();

	void readInput(const std::string&);

private:

	std::string  resolveReferences(const std::string& input, std::set<std::string>& previousRefs);
	/**
	 * \brief creates all values of spread sheet table
	 */
	void initTable();
	
	/**
	 * \brief Increments columnCount for traversing
	 */
	void incrementColumnIndex();
	/**
	 * \brief Increments rowCount when newline is read
	 */
	void incrementRowIndex();

	/**
	 * \brief Max possible row number of spreadsheet input. It is also sentinel for determining to calculate output
	 */
	constexpr static int max_row = 3;
	/**
	 * \brief Max number for a column. It is between A-Z => 65-90 in ASCII
	 */
	constexpr static char max_column = 'Z';
	
	/**
	 * \brief Value to be shown
	 */
	double outputValue = 0;
	/**
	 * \brief Data structure keeping input and output value
	 */
	std::map<std::string, std::shared_ptr<Cell>> data;
	/**
	 * \brief  input index for current column  
	 */
	char columnCount = 'A';
	/**
	 * \brief  input index for current row
	 */
	char rowCount = 1;
};


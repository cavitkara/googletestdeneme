#pragma once
#include <cctype>
#include <string>
#include <fstream>
#include <iterator>
#include <string>
#include <algorithm>
#include "Util.h"

bool isnumber(const std::string & str)
{
	if (str.size() == 0)
		return false;

	for (const char &c : str)
	{
		if (isdigit(c) == 0)
			return false;
	}
	return true;
}


double eval(std::string expr)
{
	std::string xxx; // Get Rid of Spaces
	for (int i = 0; i < expr.length(); i++)
	{
		if (expr[i] != ' ')
		{
			xxx += expr[i];
		}
	}

	std::string tok = ""; // Do parantheses first
	for (int i = 0; i < xxx.length(); i++)
	{
		if (xxx[i] == '(')
		{
			int iter = 1;
			std::string token;
			i++;
			while (true)
			{
				if (xxx[i] == '(')
				{
					iter++;
				}
				else if (xxx[i] == ')')
				{
					iter--;
					if (iter == 0)
					{
						i++;
						break;
					}
				}
				token += xxx[i];
				i++;
			}
			//cout << "(" << token << ")" << " == " << to_string(eval(token)) <<  endl;
			tok += std::to_string(eval(token));
		}
		tok += xxx[i];
	}

	for (int i = 0; i < tok.length(); i++)
	{
		if (tok[i] == '+')
		{
			//cout << tok.substr(0, i) + " + " +  tok.substr(i+1, tok.length()-i-1) << " == " << eval(tok.substr(0, i)) + eval(tok.substr(i+1, tok.length()-i-1)) << endl;
			return eval(tok.substr(0, i)) + eval(tok.substr(i + 1, tok.length() - i - 1));
		}
		else if (tok[i] == '-')
		{
			//cout << tok.substr(0, i) + " - " +  tok.substr(i+1, tok.length()-i-1) << " == " << eval(tok.substr(0, i)) - eval(tok.substr(i+1, tok.length()-i-1)) << endl;
			return eval(tok.substr(0, i)) - eval(tok.substr(i + 1, tok.length() - i - 1));
		}
	}

	for (int i = 0; i < tok.length(); i++)
	{
		if (tok[i] == '*')
		{
			//cout << tok.substr(0, i) + " * " +  tok.substr(i+1, tok.length()-i-1) << " == " << eval(tok.substr(0, i)) * eval(tok.substr(i+1, tok.length()-i-1)) << endl;
			return eval(tok.substr(0, i)) * eval(tok.substr(i + 1, tok.length() - i - 1));
		}
		else if (tok[i] == '/')
		{
			//cout << tok.substr(0, i) + " / " +  tok.substr(i+1, tok.length()-i-1) << " == " << eval(tok.substr(0, i)) / eval(tok.substr(i+1, tok.length()-i-1)) << endl;
			return eval(tok.substr(0, i)) / eval(tok.substr(i + 1, tok.length() - i - 1));
		}
	}

	//cout << stod(tok.c_str()) << endl;
	return std::stod(tok.c_str()); // Return the value...
}

bool compareFiles(const std::string& p1, const std::string& p2) 
{
	std::ifstream f1(p1, std::ifstream::binary | std::ifstream::ate);
	std::ifstream f2(p2, std::ifstream::binary | std::ifstream::ate);

	if (f1.fail() || f2.fail()) {
		return false; //file problem
	}

	if (f1.tellg() != f2.tellg()) {
		return false; //size mismatch
	}

	//seek back to beginning and use std::equal to compare contents
	f1.seekg(0, std::ifstream::beg);
	f2.seekg(0, std::ifstream::beg);
	return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
		std::istreambuf_iterator<char>(),
		std::istreambuf_iterator<char>(f2.rdbuf()));
}
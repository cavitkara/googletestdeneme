// SpreadSheet.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "Table.h"
#include <windows.h>

void SetWindow(int Width, int Height)
{
	_COORD coord;
	coord.X = Width;
	coord.Y = Height;

	_SMALL_RECT Rect;
	Rect.Top = 0;
	Rect.Left = 0;
	Rect.Bottom = Height - 1;
	Rect.Right = Width - 1;

	HANDLE Handle = GetStdHandle(STD_OUTPUT_HANDLE*3);      // Get Handle 
	SetConsoleScreenBufferSize(Handle, coord);            // Set Buffer Size 
	SetConsoleWindowInfo(Handle, TRUE, &Rect);            // Set Window Size 
}

/*
 * Driver Code for Application
 */

//int main()
//{
//	Table table;
//	table.readInput();
//	table.run();
//	table.printTableOutput4Debug();
//	table.printTableOutput();
//}



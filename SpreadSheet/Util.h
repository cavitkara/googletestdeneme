#pragma once
#include <cctype>
#include <string>

bool isnumber(const std::string & str);

double eval(std::string expr);

bool compareFiles(const std::string& p1, const std::string& p2);

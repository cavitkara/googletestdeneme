
cmake_minimum_required(VERSION 3.1...3.22)

project(
  ModernCMakeExample
  VERSION 1.0
  LANGUAGES CXX)

# Global Usage of C++ version
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)


enable_testing()
include(GoogleTest)
add_subdirectory("${PROJECT_SOURCE_DIR}/googletest")

add_subdirectory(MatrixMultiplication)
add_subdirectory(Test1)
add_subdirectory(BinomialCoefficient)
add_subdirectory(SpreadSheet)